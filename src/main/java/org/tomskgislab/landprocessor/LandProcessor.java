/*
 * LandProcessor - конвертер XML Росреестра в Shape-файлы
 * Copyright 2011 Филиппов Владислав, aka nukevlad, aka filippov70
 * filippov70@gmail.com Россия, Томск
 * 
 * Этот файл — часть LandProcessor.

   LandProcessor - свободная программа: вы можете перераспространять её и/или
   изменять её на условиях Стандартной общественной лицензии GNU в том виде,
   в каком она была опубликована Фондом свободного программного обеспечения;
   либо версии 3 лицензии, либо (по вашему выбору) любой более поздней
   версии.

   LandProcessor распространяется в надежде, что она будет полезной,
   но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной гарантии ТОВАРНОГО ВИДА
   или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее см. в Стандартной
   общественной лицензии GNU.

   Вы должны были получить копию Стандартной общественной лицензии GNU
   вместе с этой программой. Если это не так, см.
   <http://www.gnu.org/licenses/>.)
 */

package org.tomskgislab.landprocessor;

import org.geotools.data.DefaultTransaction;
import org.geotools.data.Transaction;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.data.simple.SimpleFeatureStore;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.locationtech.jts.geom.Geometry;
import org.geotools.geometry.jts.JTS;
import org.opengis.referencing.operation.MathTransform;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.referencing.operation.MathTransform;
import org.opengis.referencing.operation.TransformException;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.*;

public class LandProcessor {
    //private static Logger logger = LogManager.getLogger(LandProcessor.class);

    private SimpleFeatureType PARCEL_TYPE;
    private SimpleFeatureType QUARTAL_TYPE;
    private SimpleFeatureType ZONE_TYPE;
    private SimpleFeatureType BOUND_TYPE;
    private SimpleFeatureType REALTY_TYPE;
    private SimpleFeatureType REALTYLINE_TYPE;
    private SimpleFeatureType OMS_TYPE;
    private SimpleFeatureType CHAR_TYPE;
    private DefaultFeatureCollection parcelCollection;
    private DefaultFeatureCollection quartalCollection;
    private DefaultFeatureCollection zoneCollection;
    private DefaultFeatureCollection boundCollection;
    private DefaultFeatureCollection realtyCollection;
    private DefaultFeatureCollection realtyLineCollection;
    private DefaultFeatureCollection OMSCollection;
    private DefaultFeatureCollection charPointCollection;
    //private CadastreDictionaryLoader dicts;
    private String srs = "-";
    private CadastreMain parser = null;
    private List<String> nonSpatialParcels = null;
    //private String err = "\nДанные обработаны с ошибками. Для более подробной инфориации см. landprocessor.log";

    public LandProcessor() {
//        File pathToXSD = new File(LandProcessorConstants.PATH_TO_XSD);
//        if (pathToXSD.isDirectory())
//            dicts = new CadastreDictionaryLoader(pathToXSD);
//        else {
//            pathToXSD = new File(new File(System.getProperty("user.dir")), LandProcessorConstants.PATH_TO_XSD);
//            dicts = new CadastreDictionaryLoader(pathToXSD);
//        }
    }

    public List<String> getNonSpatialParcels () {
        return this.nonSpatialParcels;
    }

    public String getSrs() {
        return this.srs;
    }

    public boolean isEmpty() {
        if (this.quartalCollection.size() == 0) {

            return true;
        } else {
            return false;
        }
    }

    private void convertCRS(DefaultFeatureCollection features, MathTransform transform) {
        try {
            SimpleFeatureIterator feat = features.features();
            while (feat.hasNext()) {
                SimpleFeature sm = feat.next();
                Geometry geometry = (Geometry) sm.getDefaultGeometry();
                Geometry geometry2 = null;
                if ((geometry == null) || (geometry.isEmpty())) {
                    //logger.warn("Геометрия пустая, пропущена конвертация.");
                }
                else {
                    geometry2 = JTS.transform(geometry, transform);

                    sm.setDefaultGeometry(geometry2);
                }
            }
        } catch (NoSuchElementException | TransformException ex) {
            System.out.println("Convert CRS;" + ex.getMessage());
        }
    }

    protected void convertToWGS(MathTransform transform) {
        convertCRS(this.boundCollection, transform);
        convertCRS(this.OMSCollection, transform);
        convertCRS(this.parcelCollection, transform);
        convertCRS(this.quartalCollection, transform);
        convertCRS(this.realtyCollection, transform);
        convertCRS(this.realtyLineCollection, transform);
        convertCRS(this.zoneCollection, transform);
    }

    protected void mergeWith(LandProcessor anotherProcessor) throws IOException {
        this.quartalCollection.addAll(anotherProcessor.quartalCollection.collection());
        this.parcelCollection.addAll(anotherProcessor.parcelCollection.collection());
        this.zoneCollection.addAll(anotherProcessor.zoneCollection.collection());
        this.boundCollection.addAll(anotherProcessor.boundCollection.collection());
        this.realtyCollection.addAll(anotherProcessor.realtyCollection.collection());
        this.realtyLineCollection.addAll(anotherProcessor.realtyLineCollection.collection());
        this.OMSCollection.addAll(anotherProcessor.OMSCollection.collection());
        for (String record : anotherProcessor.nonSpatialParcels) {
            this.nonSpatialParcels.add(record);
        }
    }

    protected void ProcessXMLFile(File file, boolean MergeShapeFile, Boolean isSaveCharPoint) throws Exception {
        //logger.info("Начало обработки, ждите.....");
        if (parser == null)  {
            parser = new CadastreMain();
        }
        parser.parse(file, isSaveCharPoint);
        this.srs = parser.getSrs();
        if (isSaveCharPoint) {
            charPointCollection = parser.getCharPointCollection();
            CHAR_TYPE = parser.getCHAR_TYPE();
        }
        this.parcelCollection = parser.getParcelCollection();
        this.quartalCollection = parser.getQuartalCollection();
        this.zoneCollection = parser.getZoneCollection();
        this.boundCollection = parser.getBoundCollection();
        this.realtyCollection = parser.getRealtyCollection();
        this.realtyLineCollection = parser.getRealtyLineCollection();
        this.OMSCollection = parser.getOMSCollection();
        this.charPointCollection = parser.getCharPointCollection();

        this.PARCEL_TYPE = parser.getPARCEL_TYPE();
        this.QUARTAL_TYPE = parser.getQUARTAL_TYPE();
        this.ZONE_TYPE = parser.getZONEL_TYPE();
        this.BOUND_TYPE = parser.getBOUND_TYPE();
        this.REALTY_TYPE = parser.getREALTY_TYPE();
        this.REALTYLINE_TYPE = parser.getREALTYLINE_TYPE();
        this.OMS_TYPE = parser.getOMS_TYPE();
        this.CHAR_TYPE = parser.getCHAR_TYPE();
        this.nonSpatialParcels = parser.getNonSpatialParcels();
    }

    protected void saveToGeeoJSON(File FileName, String charset) {
        
    }

    protected void saveToShape(File FileName, String charset) throws IOException {

        if (this.parcelCollection.size() != 0) {
            String suffix = "_Parcels";
            saveLayerToShape(PARCEL_TYPE, parcelCollection, FileName, suffix, charset);
        }
        if (this.quartalCollection.size() != 0) {
            String suffix = "_Quartal";
            saveLayerToShape(QUARTAL_TYPE, quartalCollection, FileName, suffix, charset);
        }
        if (this.zoneCollection.size() != 0) {
            String suffix = "_Zones";
            saveLayerToShape(ZONE_TYPE, zoneCollection, FileName, suffix, charset);
        }
        if (this.boundCollection.size() != 0) {
            String suffix = "_Bounds";
            saveLayerToShape(BOUND_TYPE, boundCollection, FileName, suffix, charset);
        }
        if (this.realtyCollection.size() != 0) {
            String suffix = "_Realty";
            saveLayerToShape(REALTY_TYPE, realtyCollection, FileName, suffix, charset);
        }
        if (this.realtyLineCollection.size() != 0) {
            String suffix = "_RealtyLine";
            saveLayerToShape(REALTYLINE_TYPE, realtyLineCollection, FileName, suffix, charset);
        }
        if (this.OMSCollection.size() != 0) {
            String suffix = "_OMSPoint";
            saveLayerToShape(OMS_TYPE, OMSCollection, FileName, suffix, charset);
        }
        // точки
        if (this.charPointCollection != null && this.charPointCollection.size() != 0) {
            String suffix = "_CharPoint";
            saveLayerToShape(CHAR_TYPE, charPointCollection, FileName, suffix, charset);
        }
    }

    private void saveLayerToShape(SimpleFeatureType featureType, SimpleFeatureCollection simpleFeatureCollection, File FileName, String suffix, String charset) throws IOException {
        File RenameFile = new File(FileName.getPath().replaceAll(
                ".shp", "") + suffix + ".shp");
        FileName.renameTo(RenameFile);
        ShapefileDataStoreFactory dataStoreFactory = new ShapefileDataStoreFactory();

        Map<String, Serializable> params = new HashMap<String, Serializable>();
        params.put("url", RenameFile.toURI().toURL());
        params.put("create spatial index", Boolean.TRUE);
        ShapefileDataStore dataStore = (ShapefileDataStore) dataStoreFactory.createNewDataStore(params);
        //SimpleFeatureType QoptType = createOptimizedType(quartalCollection, QUARTAL_TYPE, 255);
        dataStore.setCharset(Charset.forName(charset));
        dataStore.createSchema(featureType);
        String typeName = dataStore.getTypeNames()[0];
        SimpleFeatureSource featureSource = dataStore.getFeatureSource(typeName);

        if (featureSource instanceof SimpleFeatureStore) {
            saveData((SimpleFeatureStore) featureSource, simpleFeatureCollection);
        } else {
            //logger.warn(QuartalTypeName + " не поддерживает чтение/запись");
        }
        // Удаление prj-файла
        File prjFile = new File(RenameFile.getPath().replaceAll(".shp", "") + ".prj");
        Files.delete(prjFile.toPath());
    }

    //    private MIFDataStore createMIFDataStore(File FileName, ReferencedEnvelope bounds) throws IOException, FactoryException {
//        MIFDataStoreFactory polygonDataStoreFactory = new MIFDataStoreFactory();
//
//        Map<String, Serializable> params = new HashMap<String, Serializable>();
//        params.put("dbtype", "mif");
//        params.put("path", FileName.getAbsolutePath());
//        params.put(MIFDataStore.PARAM_FIELDCASE, "upper");
//        params.put(MIFDataStore.PARAM_GEOMNAME, "the_geom");
//        params.put(MIFDataStore.PARAM_GEOMTYPE, "typed");
//        params.put(MIFDataStore.HCLAUSE_CHARSET, "WindowsCyrillic");
//        params.put(MIFDataStore.HCLAUSE_COORDSYS, "NonEarth Units \"m\" Bounds (" + bounds.getMinimum(0) +", " + bounds.getMinimum(1) + ") (" + bounds.getMaximum(0) +", " + bounds.getMaximum(1) + ")");
//
//        return (MIFDataStore) polygonDataStoreFactory.createNewDataStore(params);
//    }
//
//    protected void saveToMIF(File FileName, String charset) throws IOException, FactoryException {
//        if (this.parcelCollection.size() != 0) {
//            File RenameParcelFile = new File(FileName.getPath().replaceAll(
//                    ".mif", "")
//                    + "_Parcel" + ".mif");
//            FileName.renameTo(RenameParcelFile);
//
//            ReferencedEnvelope bounds = this.parcelCollection.getBounds();
//
//            MIFDataStore polygonDataStore = createMIFDataStore(RenameParcelFile, bounds);
//
//            SimpleFeatureType optType = createOptimizedType(parcelCollection, PARCEL_TYPE, 255);
//            polygonDataStore.createSchema(optType);
//            // polygonDataStore.forceSchemaCRS(CRS.decode("EPSG:28415"));
//            String ParcelTypeName = polygonDataStore.getTypeNames()[0];
//            SimpleFeatureSource parcelFeatureSource = polygonDataStore.getFeatureSource(ParcelTypeName);
//
//            if (parcelFeatureSource instanceof SimpleFeatureStore) {
//                saveData((SimpleFeatureStore) parcelFeatureSource, parcelCollection);
//            } else {
//                logger.warn(ParcelTypeName + " не поддерживает чтение/запись");
//            }
//            logger.info("Завершение конвертации участков. " + RenameParcelFile.getAbsolutePath());
//        }
//
//        if (this.quartalCollection.size() != 0) {
//            File RenameQuartalFile = new File(FileName.getPath().replaceAll(
//                    ".mif", "") + "_Qartal" + ".mif");
//            FileName.renameTo(RenameQuartalFile);
//            ReferencedEnvelope bounds = this.quartalCollection.getBounds();
//
//            MIFDataStore polygonDataStore = createMIFDataStore(RenameQuartalFile, bounds);
//
//            SimpleFeatureType optType = createOptimizedType(quartalCollection, QUARTAL_TYPE, 255);
//            polygonDataStore.createSchema(optType);
//            // polygonDataStore.forceSchemaCRS(CRS.decode("EPSG:28415"));
//            String QuartalTypeName = polygonDataStore.getTypeNames()[0];
//            SimpleFeatureSource quartalFeatureSource = polygonDataStore.getFeatureSource(QuartalTypeName);
//
//            if (quartalFeatureSource instanceof SimpleFeatureStore) {
//                saveData((SimpleFeatureStore) quartalFeatureSource, quartalCollection);
//            } else {
//                logger.warn(QuartalTypeName + " не поддерживает чтение/запись");
//            }
//            logger.info("Завершение конвертации квартала. " + RenameQuartalFile.getAbsolutePath());
//        }
//        if (this.zoneCollection.size() != 0) {
//            File RenameZoneFile = new File(FileName.getPath().replaceAll(
//                    ".mif", "") + "_Zones" + ".mif");
//            FileName.renameTo(RenameZoneFile);
//
//            ReferencedEnvelope bounds = this.zoneCollection.getBounds();
//
//            MIFDataStore polygonDataStore = createMIFDataStore(RenameZoneFile, bounds);
//
//            SimpleFeatureType optType = createOptimizedType(zoneCollection, ZONE_TYPE, 255);
//            polygonDataStore.createSchema(optType);
//            // polygonDataStore.forceSchemaCRS(CRS.decode("EPSG:28415"));
//            String ZoneTypeName = polygonDataStore.getTypeNames()[0];
//            SimpleFeatureSource zoneFeatureSource = polygonDataStore.getFeatureSource(ZoneTypeName);
//
//            if (zoneFeatureSource instanceof SimpleFeatureStore) {
//                saveData((SimpleFeatureStore) zoneFeatureSource, zoneCollection);
//            } else {
//                logger.warn(ZoneTypeName + " не поддерживает чтение/запись");
//            }
//
//            logger.info("Завершение конвертации зон. " + RenameZoneFile.getAbsolutePath());
//        }
//        if (this.boundCollection.size() != 0) {
//            File RenameBoundFile = new File(FileName.getPath().replaceAll(
//                    ".mif", "") + "_Bounds" + ".mif");
//            FileName.renameTo(RenameBoundFile);
//
//            ReferencedEnvelope bounds = this.boundCollection.getBounds();
//
//            MIFDataStore polygonDataStore = createMIFDataStore(RenameBoundFile, bounds);
//
//            SimpleFeatureType optType = createOptimizedType(boundCollection, BOUND_TYPE, 255);
//            polygonDataStore.createSchema(optType);
//            // polygonDataStore.forceSchemaCRS(CRS.decode("EPSG:28415"));
//            String BoundTypeName = polygonDataStore.getTypeNames()[0];
//            SimpleFeatureSource boundFeatureSource = polygonDataStore.getFeatureSource(BoundTypeName);
//
//            if (boundFeatureSource instanceof SimpleFeatureStore) {
//                saveData((SimpleFeatureStore) boundFeatureSource, boundCollection);
//            } else {
//                logger.warn(BoundTypeName + " не поддерживает чтение/запись");
//            }
//
//            logger.info("Завершение конвертации границ. " + RenameBoundFile.getAbsolutePath());
//        }
//        if (this.realtyCollection.size() != 0) {
//            File RenameRealtyFile = new File(FileName.getPath().replaceAll(
//                    ".mif", "") + "_Realty" + ".mif");
//            FileName.renameTo(RenameRealtyFile);
//
//            ReferencedEnvelope bounds = this.realtyCollection.getBounds();
//
//            MIFDataStore realtydDataStore = createMIFDataStore(RenameRealtyFile, bounds);
//            SimpleFeatureType BoptType = createOptimizedType(realtyCollection, REALTY_TYPE, 255);
//            realtydDataStore.createSchema(BoptType);
//            String RealtyTypeName = realtydDataStore.getTypeNames()[0];
//            SimpleFeatureSource realtyFeatureSource = realtydDataStore
//                    .getFeatureSource(RealtyTypeName);
//
//            if (realtyFeatureSource instanceof SimpleFeatureStore) {
//                saveData((SimpleFeatureStore) realtyFeatureSource, realtyCollection);
//            } else {
//                logger.warn(RealtyTypeName + " не поддерживает чтение/запись");
//            }
//
//            logger.info("Завершение конвертации ОКС. " + RenameRealtyFile.getAbsolutePath());
//        }
//
//        if (this.realtyLineCollection.size() != 0) {
//            File RenameFile = new File(FileName.getPath().replaceAll(
//                    ".mif", "") + "_RealtyLine" + ".mif");
//            FileName.renameTo(RenameFile);
//
//            ReferencedEnvelope bounds = this.realtyLineCollection.getBounds();
//
//            MIFDataStore dataStore = createMIFDataStore(RenameFile, bounds);
//            SimpleFeatureType BoptType = createOptimizedType(realtyLineCollection, REALTYLINE_TYPE, 255);
//            dataStore.createSchema(BoptType);
//            String typeName = dataStore.getTypeNames()[0];
//            SimpleFeatureSource featureSource = dataStore.getFeatureSource(typeName);
//
//            if (featureSource instanceof SimpleFeatureStore) {
//                saveData((SimpleFeatureStore) featureSource, realtyLineCollection);
//            } else {
//                logger.warn(typeName + " не поддерживает чтение/запись");
//            }
//
//            logger.info("Завершение конвертации линейных ОКС. " + RenameFile.getAbsolutePath());
//        }
//
//        if (this.OMSCollection.size() != 0) {
//            File RenameOMSFile = new File(FileName.getPath().replaceAll(
//                    ".mif", "") + "_OMSPoint" + ".mif");
//            FileName.renameTo(RenameOMSFile);
//
//            ReferencedEnvelope bounds = this.OMSCollection.getBounds();
//
//            MIFDataStore OMSDataStore = createMIFDataStore(RenameOMSFile, bounds);
//
//            SimpleFeatureType BoptType = createOptimizedType(OMSCollection, OMS_TYPE, 255);
//            OMSDataStore.createSchema(BoptType);
//            String OMSTypeName = OMSDataStore.getTypeNames()[0];
//            SimpleFeatureSource OMSFeatureSource = OMSDataStore
//                    .getFeatureSource(OMSTypeName);
//
//            if (OMSFeatureSource instanceof SimpleFeatureStore) {
//                saveData((SimpleFeatureStore) OMSFeatureSource, this.OMSCollection);
//            } else {
//                logger.warn(OMSTypeName + " не поддерживает чтение/запись");
//            }
//
//            logger.info("Завершение конвертации пунктов ОМС. " + RenameOMSFile.getAbsolutePath());
//        }
//    }
//

    private SimpleFeatureType createOptimizedType(SimpleFeatureCollection Collection, SimpleFeatureType type, int maxLen) {
        SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
        builder.setCRS(type.getCoordinateReferenceSystem());
        builder.setName(type.getName());
        builder.add("Geometry", type.getDescriptor(0).getType().getBinding());

        int count = type.getAttributeCount();
        for (int i = 1; i < count; i++) {
            int maxLength = calcMaxLength(i, Collection.features());
            Class<?> cl = type.getDescriptor(i).getType().getBinding();
            builder.length(Math.min(maxLen, maxLength)).add(type.getDescriptor(i).getLocalName(), cl);
        }
        try {
            return builder.buildFeatureType();
        } catch (Exception ex) {
            //logger.error("Не удалость создать пространственный объект. ", ex);
            return null;
        }
    }

    private int calcMaxLength(int i, SimpleFeatureIterator iterator) {
        int maxLength = 2;
        try {
            while (iterator.hasNext()) {
                SimpleFeature feature = iterator.next();
                Object o = feature.getAttribute(i);
                if (o instanceof String) {
                    String s = (String) o;
                    if (maxLength < s.length())
                        maxLength = s.length();
//                    if (maxLength == 0)
//                    	maxLength = 1;
                }
            }
        } finally {
            iterator.close();
        }
        //logger.info("Max length is " + maxLength);
        return maxLength;
    }

    private void saveData(SimpleFeatureStore FeatureStore, SimpleFeatureCollection Collection) throws IOException {
        Transaction transaction = new DefaultTransaction("create");
        FeatureStore.setTransaction(transaction);
        try {
            FeatureStore.addFeatures(Collection);
            transaction.commit();
            System.out.println("Успешное сохранение. " + FeatureStore.getName());
            //logger.info("Успешное сохранение." );

        } catch (Exception ex) {
            System.out.println("Сохранение не удалось. " + FeatureStore.getName());
            //logger.error(
            //"Ошибка при добавлении в shapefile.\nОткат изменений в shapefile, но файл возможно создан.",
            //   ex);
            transaction.rollback();

        } finally {
            transaction.close();
            //logger.info("Конец сохранения данных.");
        }
    }

}//End of the class LandProcessor
