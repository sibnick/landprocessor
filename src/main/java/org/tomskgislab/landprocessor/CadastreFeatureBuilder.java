/*
LandProcessor - конвертер XML Росреестра в распространённые ГИС-форматы
Copyright 2011 Филиппов Владислав, aka nukevlad, aka filippov70
filippov70@gmail.com Россия, Томск, Кольцово
MIT License
Данная лицензия разрешает лицам, получившим копию данного программного
обеспечения и сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»),
безвозмездно использовать Программное Обеспечение без ограничений, включая неограниченное
право на использование, копирование, изменение, слияние, публикацию, распространение,
сублицензирование и/или продажу копий Программного Обеспечения, а также лицам, которым
предоставляется данное Программное Обеспечение, при соблюдении следующих условий:
Указанное выше уведомление об авторском праве и данные условия должны быть включены во
все копии или значимые части данного Программного Обеспечения.

ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ,
ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ
ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. НИ В КАКОМ
СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА УЩЕРБ ИЛИ
ПО ИНЫМ ТРЕБОВАНИЯМ, В ТОМ ЧИСЛЕ, ПРИ ДЕЙСТВИИ КОНТРАКТА, ДЕЛИКТЕ ИЛИ ИНОЙ СИТУАЦИИ,
ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 */
package org.tomskgislab.landprocessor;

import org.locationtech.jts.geom.Point;
import org.geotools.feature.simple.*;
import org.geotools.referencing.crs.DefaultEngineeringCRS;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.MultiPolygon;
import org.opengis.feature.simple.SimpleFeatureType;



/**
 * Cоздатель кадастровых объектов.
 * Содержит описание пространственных объектов.
 * @author Филиппов Владислав
 */
public class CadastreFeatureBuilder {
 
//private static Logger logger = LogManager.getLogger( CadastreFeatureBuilder.class );

    public enum CadastreFeatureTypes {
        CharacterPoint,     // Характерная точка
        Parcel,             // Участок
        Quartal,            // Кадастровый квартал
        Zones,              // Территориальные зоны и Зоны с особыми условиями
        Bounds,             // Границы муниципальных образований и поселений
        Realty,             // ОКС
        RealtyLine,         // ОКС
        OMSPoint            // Пункты ОМС
    }
    
    /**
     * Создатель типов пространственных объектов.

     * @param FeatureType тип создаваемого объекта (enum)
     * @return Созданая фича 
     */
    public static SimpleFeatureType createFeatureType(CadastreFeatureTypes FeatureType) {

        SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
        builder.setCRS(DefaultEngineeringCRS.CARTESIAN_2D);

        switch (FeatureType)  {
        	// По просьбам трудящихся точки
            case CharacterPoint :
                builder.setName("ChPoint");
                builder.add("the_geom", Point.class);
                builder.length(30).add("name", String.class);
                builder.length(40).add("cadnum", String.class);
                builder.length(15).add("quartal", String.class);
                builder.length(20).add("ordnumb", String.class);
                builder.length(10).add("delta", String.class);
                builder.length(150).add("zacr", String.class);
                break;
            case Parcel :
                builder.setName("Parcel");
                builder.add("the_geom", MultiPolygon.class); //
                builder.length(100).add("quartal", String.class);
                builder.length(254).add("cadnumber", String.class);

                builder.length(254).add("parentnum", String.class); // comoncadn
                builder.length(254).add("category", String.class);
                builder.add("utildoc", String.class);
                builder.add("utiltype", String.class);
                builder.add("utilgrreg", String.class);
                // В схеме это "Name", хотя обозначает тип
                builder.length(100).add("type", String.class);
                builder.length(100).add("state", String.class);
                builder.length(100).add("area", Double.class);
                builder.length(100).add("inaccur", Double.class);
                // Address v3
//                builder.length(254).add("okato", String.class);
//                builder.length(254).add("oktmo", String.class);
//                builder.length(254).add("kladr", String.class);
//                builder.length(254).add("postcode", String.class);
                builder.length(254).add("adrtype", String.class);
                builder.length(254).add("location", String.class);
                builder.length(254).add("adr1", String.class);

                builder.length(254).add("street", String.class);
                builder.length(254).add("level1", String.class);
                builder.length(254).add("level2", String.class);
                builder.length(254).add("level3", String.class);
                builder.length(254).add("apartment", String.class);
                builder.add("other", String.class);
                builder.add("note", String.class);
                builder.length(50).add("respdate", String.class);
                // 30/06/2016
//                builder.length(254).add("prevnumb", String.class);
//                builder.length(254).add("addnumber", String.class);
//                builder.length(254).add("delnumber", String.class);
//                builder.length(254).add("cadeng", String.class); // 41
//                builder.length(254).add("respdate", String.class);

                break;
            case Quartal :
                builder.setName("Quartal");
                builder.add("the_geom", MultiPolygon.class); // the_geom:Point:srid=4326
                builder.length(254).add("name", String.class);
                builder.minOccurs(0).maxOccurs(2).add("area", Double.class);
                builder.length(25).add("areaunit", String.class);
                builder.length(254).add("coordsys", String.class);
                builder.length(254).add("cadnumber", String.class);
                builder.length(254).add("respdate", String.class);
                builder.length(20).add("srs", String.class);
                break;
            case Realty:
                builder.setName("Realty");
            	builder.add("the_geom", MultiPolygon.class);
            	builder.length(150).add("name", String.class);
                builder.length(254).add("purpose", String.class);
                builder.length(254).add("cadnumber", String.class);
                builder.length(254).add("permuses", String.class);
                builder.length(254).add("cadcost", String.class);
                builder.length(15).add("area", Double.class);

                // Address v3
//                builder.length(254).add("okato", String.class);
//                builder.length(254).add("oktmo", String.class);
//                builder.length(254).add("kladr", String.class);
//                builder.length(254).add("postcode", String.class);
                builder.length(254).add("adr1", String.class);

                builder.length(254).add("street", String.class);
                builder.length(254).add("level1", String.class);
                builder.length(254).add("level2", String.class);
                builder.length(254).add("level3", String.class);
                builder.length(254).add("apart", String.class);
                builder.add("other", String.class);
                builder.add("note", String.class);
                //builder.length(254).add("respdate", String.class);
                break;
            case RealtyLine:
                builder.setName("RealtyL");
            	builder.add("the_geom", LineString.class);
            	builder.length(150).add("name", String.class);
                builder.length(254).add("object", String.class);
                builder.length(254).add("cadnumber", String.class);
                builder.length(254).add("permuses", String.class);
                builder.length(254).add("cadcost", String.class);
                builder.length(100).add("area", Double.class);

                // Address v3
//                builder.length(254).add("okato", String.class);
//                builder.length(254).add("oktmo", String.class);
//                builder.length(254).add("kladr", String.class);
//                builder.length(254).add("postcode", String.class);
                builder.length(254).add("adr1", String.class);

                builder.length(254).add("street", String.class);
                builder.length(254).add("level1", String.class);
                builder.length(254).add("level2", String.class);
                builder.length(254).add("level3", String.class);
                builder.length(254).add("apart", String.class);
                builder.add("other", String.class);
                builder.add("note", String.class);
                //builder.length(254).add("respdate", String.class);
                builder.setDefaultGeometry( "the_geom" ); // hack
                break;
            case Zones :
                builder.setName("Zones");
                builder.add("the_geom", MultiPolygon.class);
                builder.length(150).add("name", String.class);
                builder.length(50).add("number", String.class);
                builder.add("descr", String.class);
                builder.add("type", String.class);
                builder.add("regnumber", String.class);
                builder.add("typebound", String.class);
                builder.add("other", String.class);
                builder.add("quartal", String.class);
                builder.add("respdate", String.class);
                break;
            case Bounds :
                builder.setName("Bounds");
                builder.add("the_geom", MultiPolygon.class);
                builder.length(150).add("type", String.class);
                builder.length(50).add("typebound", String.class);
                builder.add("accnumber", String.class); // regnum
                builder.add("descr", String.class);
                builder.add("regdate", String.class);
                builder.add("quartal", String.class);
                builder.add("respdate", String.class);

                break;
            case OMSPoint :
                builder.setName("OMSPoint");
                builder.add("the_geom", Point.class);
                builder.length(150).add("name", String.class);
                builder.length(254).add("quartal", String.class);
                builder.length(254).add("pnmb", String.class);
                builder.length(254).add("pname", String.class);
                builder.length(254).add("pclass", String.class);
                builder.length(25).add("x", String.class);
                builder.length(25).add("y", String.class);
                builder.length(254).add("respdate", String.class);
                break;

            default:
               //logger.warn();
                System.out.println("Нет обработчика для данного типа: " + FeatureType.toString());
               return  null;
        }

        // build the type
        try {
            return builder.buildFeatureType();
        }
        catch (Exception ex) {
            //logger.error("Не удалость создать пространственный объект. ", ex);
            System.err.println("Failed create feature type. "+ ex.getMessage());
            return null;
        }

    }
}
