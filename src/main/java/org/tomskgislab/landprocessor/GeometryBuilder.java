/*
 * LandProcessor - конвертер XML Росреестра в Shape-файлы
 * Copyright 2011 Филиппов Владислав, aka nukevlad, aka filippov70
 * filippov70@gmail.com Россия, Томск
 * 
 * Этот файл — часть LandProcessor.

   LandProcessor - свободная программа: вы можете перераспространять её и/или
   изменять её на условиях Стандартной общественной лицензии GNU в том виде,
   в каком она была опубликована Фондом свободного программного обеспечения;
   либо версии 3 лицензии, либо (по вашему выбору) любой более поздней
   версии.

   LandProcessor распространяется в надежде, что она будет полезной,
   но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной гарантии ТОВАРНОГО ВИДА
   или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее см. в Стандартной
   общественной лицензии GNU.

   Вы должны были получить копию Стандартной общественной лицензии GNU
   вместе с этой программой. Если это не так, см.
   <http://www.gnu.org/licenses/>.)
 */

package org.tomskgislab.landprocessor;

import java.util.List;

import org.geotools.geometry.jts.JTSFactoryFinder;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateList;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.CoordinateSequenceFactory;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.geom.MultiLineString;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.geom.impl.CoordinateArraySequence;
import org.locationtech.jts.geom.impl.CoordinateArraySequenceFactory;

import java.math.BigDecimal;

public class GeometryBuilder {

    //private static Logger logger = LogManager.getLogger(GeometryBuilder.class);
    private static GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
    //private static CoordinateList coords; // Координаты контура
    //private static LinearRing mainRing; // Основной контур

    /**
     * Создаёт замкнутый конур на основе списка (CoordinateList) координат
     *
     * @return Контур JTS
     */
    public static LinearRing createLinearRing(CoordinateList coords) {
        try {
            if ((coords != null) && (coords.size() > 0)) {
                //Coordinate c = coords.getCoordinate(0);
                //coords.add(c, true);
                Coordinate[] arr = coords.toCoordinateArray();
                CoordinateSequenceFactory fact = CoordinateArraySequenceFactory.
                      instance();
                CoordinateSequence cseq = fact.create(arr);
                LinearRing ring = new LinearRing(cseq, geometryFactory);
                // innerRingsCount++;
                return ring;
            } else {
                return null;
            }
        } catch (Exception ex) {
            //logger.error("Ошибка создания LinearRing. ", ex);
            return null;
        }
    }

    public static LineString createLinearString(CoordinateList coords) {
        try {
            if ((coords != null) && (coords.size() > 0)) {
                Coordinate[] arr = coords.toCoordinateArray();
                CoordinateSequenceFactory fact = CoordinateArraySequenceFactory.
                      instance();
                CoordinateSequence cseq = fact.create(arr);

                LineString ring = new LineString(cseq, geometryFactory);
                // innerRingsCount++;
                return ring;
            } else {
                return null;
            }
        } catch (Exception ex) {
            //logger.error("Ошибка создания LinearRing. ", ex);
            return null;
        }
    }

    /**
     * Создаёт полигон на основе основного и списка внутренних контуров.
     * Содержит проверку на наличие в списке внутренних контуров контура,
     * который больше внешнего. При наличии такого контура, он меняется местами
     * с основным.
     *
     * @param linearRing Список внутренних контуров
     * @return Полигон JTS
     */
    public static Polygon createPolygon(List<LinearRing> linearRing) {
        try {
            LinearRing mainRing = null;
            if ((linearRing != null) && (linearRing.size() > 0)) {
                int maxAreaIndex = 0;
                // Если один контур, то создаём сразу.
                if (linearRing.size() == 1) {
                    return new Polygon(linearRing.get(0), null, geometryFactory);
                }
                //int h = linearRing.size() - 1;
                //logger.info("Обрабатывается полигон с 'дырками', количеством " + h);
                Polygon p = new Polygon(linearRing.get(0), null, geometryFactory);
                double maxArea = p.getArea();
                //logger.info("Площадь 'основного' контура " + maxArea);
                for (int i = 1; i < linearRing.size(); i++) {
                    LinearRing lr = linearRing.get(i);
                    Polygon pHole = new Polygon(lr, null, geometryFactory);
                    //logger.info("Площадь " + i + "контура " + pHole.getArea());
                    if (maxArea < pHole.getArea()) {
                        maxArea = pHole.getArea();
                        maxAreaIndex = i;
                        //logger.info("Основной контур теперь " + i);
                    }
                }

                mainRing = linearRing.remove(maxAreaIndex);
                Polygon pp = new Polygon(mainRing, null, geometryFactory);
                //logger.info("Площадь 'основного' контура " + pp.getArea());
                if (maxAreaIndex > 0) {
                    //logger.info("Основной контур объекта в XML был в списке не первый! Основной контур заменён. "
                    //        + "Индекс основного контура был " + maxAreaIndex);
                }
                LinearRing[] lra = new LinearRing[linearRing.size()];
                for (int i = 0; i < linearRing.size(); i++) {
                    lra[i] = linearRing.get(i);
                }
                return new Polygon(mainRing, lra, geometryFactory);
            }
            return null;
        } catch (Exception ex) {
            //logger.error("Ошибка создания полигона. ", ex);
            return null;
        }
    }

    public static Point createPoint(BigDecimal X, BigDecimal Y) {
        try {
            double x = Y.doubleValue();
            double y = X.doubleValue();
            Coordinate[] coordinate = new Coordinate[1];
            coordinate[0] = new Coordinate(x, y);
            CoordinateSequence coordinates = new CoordinateArraySequence(coordinate);
            return new Point(coordinates, geometryFactory);

        } catch (Exception ex) {
            return null;
        }
    }

    public static MultiLineString createLine(List<CoordinateList> CoordList) {
        try {
            LineString[] ls = new LineString[CoordList.size()];
            for (int i = 0; i < CoordList.size(); i++) {
                LineString l = createLinearString(CoordList.get(i));
                ls[i] = l;
            }
            MultiLineString line = new MultiLineString(ls, geometryFactory);
            return line;

        } catch (Exception ex) {
            return null;
        }
    }

    public static LineString createLineString(CoordinateList CoordList) {
        try {
            LineString l = createLinearString(CoordList);
            return l;
        } catch (Exception ex) {
            return null;
        }
    }

    public static GeometryFactory getGeometryFactory() {
        return geometryFactory;
    }
}
