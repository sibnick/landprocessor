//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.12.21 at 05:58:11 PM GMT+07:00 
//


package extract.kpt;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Сведения об объекте недвижимости (вид, кадастровый номер, адрес полный)
 * 
 * <p>Java class for BaseData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BaseData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="land_record" type="{}DataObjectLand"/>
 *         &lt;element name="room_record" type="{}DataObjectRoom"/>
 *         &lt;element name="car_parking_space_record" type="{}DataObjectCarParkingSpace"/>
 *         &lt;element name="property_complex_record" type="{}DataObjectPropertyComplex"/>
 *         &lt;element name="unified_real_estate_complex_record" type="{}DataObjectConstruction"/>
 *         &lt;element name="object_under_construction_record" type="{}DataObjectConstruction"/>
 *         &lt;element name="build_record" type="{}DataObjectBuild"/>
 *         &lt;element name="construction_record" type="{}DataObjectConstruction"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BaseData", propOrder = {
    "landRecord",
    "roomRecord",
    "carParkingSpaceRecord",
    "propertyComplexRecord",
    "unifiedRealEstateComplexRecord",
    "objectUnderConstructionRecord",
    "buildRecord",
    "constructionRecord"
})
public class BaseData {

    @XmlElement(name = "land_record")
    protected DataObjectLand landRecord;
    @XmlElement(name = "room_record")
    protected DataObjectRoom roomRecord;
    @XmlElement(name = "car_parking_space_record")
    protected DataObjectCarParkingSpace carParkingSpaceRecord;
    @XmlElement(name = "property_complex_record")
    protected DataObjectPropertyComplex propertyComplexRecord;
    @XmlElement(name = "unified_real_estate_complex_record")
    protected DataObjectConstruction unifiedRealEstateComplexRecord;
    @XmlElement(name = "object_under_construction_record")
    protected DataObjectConstruction objectUnderConstructionRecord;
    @XmlElement(name = "build_record")
    protected DataObjectBuild buildRecord;
    @XmlElement(name = "construction_record")
    protected DataObjectConstruction constructionRecord;

    /**
     * Gets the value of the landRecord property.
     * 
     * @return
     *     possible object is
     *     {@link DataObjectLand }
     *     
     */
    public DataObjectLand getLandRecord() {
        return landRecord;
    }

    /**
     * Sets the value of the landRecord property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataObjectLand }
     *     
     */
    public void setLandRecord(DataObjectLand value) {
        this.landRecord = value;
    }

    /**
     * Gets the value of the roomRecord property.
     * 
     * @return
     *     possible object is
     *     {@link DataObjectRoom }
     *     
     */
    public DataObjectRoom getRoomRecord() {
        return roomRecord;
    }

    /**
     * Sets the value of the roomRecord property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataObjectRoom }
     *     
     */
    public void setRoomRecord(DataObjectRoom value) {
        this.roomRecord = value;
    }

    /**
     * Gets the value of the carParkingSpaceRecord property.
     * 
     * @return
     *     possible object is
     *     {@link DataObjectCarParkingSpace }
     *     
     */
    public DataObjectCarParkingSpace getCarParkingSpaceRecord() {
        return carParkingSpaceRecord;
    }

    /**
     * Sets the value of the carParkingSpaceRecord property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataObjectCarParkingSpace }
     *     
     */
    public void setCarParkingSpaceRecord(DataObjectCarParkingSpace value) {
        this.carParkingSpaceRecord = value;
    }

    /**
     * Gets the value of the propertyComplexRecord property.
     * 
     * @return
     *     possible object is
     *     {@link DataObjectPropertyComplex }
     *     
     */
    public DataObjectPropertyComplex getPropertyComplexRecord() {
        return propertyComplexRecord;
    }

    /**
     * Sets the value of the propertyComplexRecord property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataObjectPropertyComplex }
     *     
     */
    public void setPropertyComplexRecord(DataObjectPropertyComplex value) {
        this.propertyComplexRecord = value;
    }

    /**
     * Gets the value of the unifiedRealEstateComplexRecord property.
     * 
     * @return
     *     possible object is
     *     {@link DataObjectConstruction }
     *     
     */
    public DataObjectConstruction getUnifiedRealEstateComplexRecord() {
        return unifiedRealEstateComplexRecord;
    }

    /**
     * Sets the value of the unifiedRealEstateComplexRecord property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataObjectConstruction }
     *     
     */
    public void setUnifiedRealEstateComplexRecord(DataObjectConstruction value) {
        this.unifiedRealEstateComplexRecord = value;
    }

    /**
     * Gets the value of the objectUnderConstructionRecord property.
     * 
     * @return
     *     possible object is
     *     {@link DataObjectConstruction }
     *     
     */
    public DataObjectConstruction getObjectUnderConstructionRecord() {
        return objectUnderConstructionRecord;
    }

    /**
     * Sets the value of the objectUnderConstructionRecord property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataObjectConstruction }
     *     
     */
    public void setObjectUnderConstructionRecord(DataObjectConstruction value) {
        this.objectUnderConstructionRecord = value;
    }

    /**
     * Gets the value of the buildRecord property.
     * 
     * @return
     *     possible object is
     *     {@link DataObjectBuild }
     *     
     */
    public DataObjectBuild getBuildRecord() {
        return buildRecord;
    }

    /**
     * Sets the value of the buildRecord property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataObjectBuild }
     *     
     */
    public void setBuildRecord(DataObjectBuild value) {
        this.buildRecord = value;
    }

    /**
     * Gets the value of the constructionRecord property.
     * 
     * @return
     *     possible object is
     *     {@link DataObjectConstruction }
     *     
     */
    public DataObjectConstruction getConstructionRecord() {
        return constructionRecord;
    }

    /**
     * Sets the value of the constructionRecord property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataObjectConstruction }
     *     
     */
    public void setConstructionRecord(DataObjectConstruction value) {
        this.constructionRecord = value;
    }

}
