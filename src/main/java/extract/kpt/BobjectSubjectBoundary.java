//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.12.21 at 05:58:11 PM GMT+07:00 
//


package extract.kpt;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Общие сведения о границе между субъектами Российской Федерации
 * 
 * <p>Java class for BobjectSubjectBoundary complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BobjectSubjectBoundary">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="b_object" type="{}Bobject"/>
 *         &lt;element name="neighbour_regions" type="{}NeighbourRegions"/>
 *         &lt;element name="description" type="{}MultiLiner" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BobjectSubjectBoundary", propOrder = {
    "bObject",
    "neighbourRegions",
    "description"
})
public class BobjectSubjectBoundary {

    @XmlElement(name = "b_object", required = true)
    protected Bobject bObject;
    @XmlElement(name = "neighbour_regions", required = true)
    protected NeighbourRegions neighbourRegions;
    protected String description;

    /**
     * Gets the value of the bObject property.
     * 
     * @return
     *     possible object is
     *     {@link Bobject }
     *     
     */
    public Bobject getBObject() {
        return bObject;
    }

    /**
     * Sets the value of the bObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link Bobject }
     *     
     */
    public void setBObject(Bobject value) {
        this.bObject = value;
    }

    /**
     * Gets the value of the neighbourRegions property.
     * 
     * @return
     *     possible object is
     *     {@link NeighbourRegions }
     *     
     */
    public NeighbourRegions getNeighbourRegions() {
        return neighbourRegions;
    }

    /**
     * Sets the value of the neighbourRegions property.
     * 
     * @param value
     *     allowed object is
     *     {@link NeighbourRegions }
     *     
     */
    public void setNeighbourRegions(NeighbourRegions value) {
        this.neighbourRegions = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

}
