//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.12.21 at 05:58:11 PM GMT+07:00 
//


package extract.kpt;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Единое землепользование (кадастровый номер)
 * 
 * <p>Java class for CommonLandCadNumbers complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommonLandCadNumbers">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="common_land" type="{}CommonLandCadNumber"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommonLandCadNumbers", propOrder = {
    "commonLand"
})
public class CommonLandCadNumbers {

    @XmlElement(name = "common_land", required = true)
    protected CommonLandCadNumber commonLand;

    /**
     * Gets the value of the commonLand property.
     * 
     * @return
     *     possible object is
     *     {@link CommonLandCadNumber }
     *     
     */
    public CommonLandCadNumber getCommonLand() {
        return commonLand;
    }

    /**
     * Sets the value of the commonLand property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommonLandCadNumber }
     *     
     */
    public void setCommonLand(CommonLandCadNumber value) {
        this.commonLand = value;
    }

}
