//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.12.21 at 05:58:11 PM GMT+07:00 
//


package extract.kpt;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Единое землепользование (Кадастровые номера земельных участков, входящих в состав единого землепользования)
 * 
 * <p>Java class for CommonLand complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommonLand">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="common_land_parts">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="included_cad_numbers" type="{}IncludedCadNumbers"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommonLand", propOrder = {
    "commonLandParts"
})
public class CommonLand {

    @XmlElement(name = "common_land_parts", required = true)
    protected CommonLand.CommonLandParts commonLandParts;

    /**
     * Gets the value of the commonLandParts property.
     * 
     * @return
     *     possible object is
     *     {@link CommonLand.CommonLandParts }
     *     
     */
    public CommonLand.CommonLandParts getCommonLandParts() {
        return commonLandParts;
    }

    /**
     * Sets the value of the commonLandParts property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommonLand.CommonLandParts }
     *     
     */
    public void setCommonLandParts(CommonLand.CommonLandParts value) {
        this.commonLandParts = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="included_cad_numbers" type="{}IncludedCadNumbers"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "includedCadNumbers"
    })
    public static class CommonLandParts {

        @XmlElement(name = "included_cad_numbers", required = true)
        protected IncludedCadNumbers includedCadNumbers;

        /**
         * Gets the value of the includedCadNumbers property.
         * 
         * @return
         *     possible object is
         *     {@link IncludedCadNumbers }
         *     
         */
        public IncludedCadNumbers getIncludedCadNumbers() {
            return includedCadNumbers;
        }

        /**
         * Sets the value of the includedCadNumbers property.
         * 
         * @param value
         *     allowed object is
         *     {@link IncludedCadNumbers }
         *     
         */
        public void setIncludedCadNumbers(IncludedCadNumbers value) {
            this.includedCadNumbers = value;
        }

    }

}
