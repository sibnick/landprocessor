//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.12.21 at 05:58:11 PM GMT+07:00 
//


package extract.kpt;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Сведения о правопритязаниях
 * 
 * <p>Java class for ClaimRecordsOutType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClaimRecordsOutType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="claim_record" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="claim_data">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="urd_type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClaimRecordsOutType", propOrder = {
    "claimRecord"
})
public class ClaimRecordsOutType {

    @XmlElement(name = "claim_record", required = true)
    protected List<ClaimRecordsOutType.ClaimRecord> claimRecord;

    /**
     * Gets the value of the claimRecord property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the claimRecord property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClaimRecord().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClaimRecordsOutType.ClaimRecord }
     * 
     * 
     */
    public List<ClaimRecordsOutType.ClaimRecord> getClaimRecord() {
        if (claimRecord == null) {
            claimRecord = new ArrayList<ClaimRecordsOutType.ClaimRecord>();
        }
        return this.claimRecord;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="claim_data">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="urd_type" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "claimData"
    })
    public static class ClaimRecord {

        @XmlElement(name = "claim_data", required = true)
        protected ClaimRecordsOutType.ClaimRecord.ClaimData claimData;

        /**
         * Gets the value of the claimData property.
         * 
         * @return
         *     possible object is
         *     {@link ClaimRecordsOutType.ClaimRecord.ClaimData }
         *     
         */
        public ClaimRecordsOutType.ClaimRecord.ClaimData getClaimData() {
            return claimData;
        }

        /**
         * Sets the value of the claimData property.
         * 
         * @param value
         *     allowed object is
         *     {@link ClaimRecordsOutType.ClaimRecord.ClaimData }
         *     
         */
        public void setClaimData(ClaimRecordsOutType.ClaimRecord.ClaimData value) {
            this.claimData = value;
        }


        /**
         * 
         * 
         *             
         * 
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="urd_type" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "urdType"
        })
        public static class ClaimData {

            @XmlElement(name = "urd_type", required = true)
            protected String urdType;

            /**
             * Gets the value of the urdType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUrdType() {
                return urdType;
            }

            /**
             * Sets the value of the urdType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUrdType(String value) {
                this.urdType = value;
            }

        }

    }

}
