//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.12.21 at 05:58:11 PM GMT+07:00 
//


package extract.kpt;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Местоположение (до уровня населенного пункта)
 * 
 * <p>Java class for LocationCity complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LocationCity">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="level_settlement" type="{}AddressCity"/>
 *         &lt;element name="position_description" type="{}MultiLiner" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocationCity", propOrder = {
    "levelSettlement",
    "positionDescription"
})
public class LocationCity {

    @XmlElement(name = "level_settlement", required = true)
    protected AddressCity levelSettlement;
    @XmlElement(name = "position_description")
    protected String positionDescription;

    /**
     * Gets the value of the levelSettlement property.
     * 
     * @return
     *     possible object is
     *     {@link AddressCity }
     *     
     */
    public AddressCity getLevelSettlement() {
        return levelSettlement;
    }

    /**
     * Sets the value of the levelSettlement property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressCity }
     *     
     */
    public void setLevelSettlement(AddressCity value) {
        this.levelSettlement = value;
    }

    /**
     * Gets the value of the positionDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPositionDescription() {
        return positionDescription;
    }

    /**
     * Sets the value of the positionDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPositionDescription(String value) {
        this.positionDescription = value;
    }

}
