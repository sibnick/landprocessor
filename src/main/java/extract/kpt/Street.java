//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.12.21 at 05:58:11 PM GMT+07:00 
//


package extract.kpt;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Улица
 * 
 * <p>Java class for Street complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Street">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="type_street" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="name_street" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Street", propOrder = {
    "typeStreet",
    "nameStreet"
})
public class Street {

    @XmlElement(name = "type_street", required = true)
    protected String typeStreet;
    @XmlElement(name = "name_street", required = true)
    protected String nameStreet;

    /**
     * Gets the value of the typeStreet property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeStreet() {
        return typeStreet;
    }

    /**
     * Sets the value of the typeStreet property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeStreet(String value) {
        this.typeStreet = value;
    }

    /**
     * Gets the value of the nameStreet property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameStreet() {
        return nameStreet;
    }

    /**
     * Sets the value of the nameStreet property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameStreet(String value) {
        this.nameStreet = value;
    }

}
