//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.12.21 at 05:58:11 PM GMT+07:00 
//


package extract.kpt;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Машино-место
 * 
 * <p>Java class for CarParkingSpaceRecord complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CarParkingSpaceRecord">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="object" type="{}ObjectType"/>
 *         &lt;element name="params" type="{}Area"/>
 *         &lt;element name="address_room" type="{}AddressLocationCarParkingSpace"/>
 *         &lt;element name="right_record" type="{}RightRecordIndivid"/>
 *         &lt;element name="restrict_records" type="{}RestrictRecordsIndividNoPart" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CarParkingSpaceRecord", propOrder = {
    "object",
    "params",
    "addressRoom",
    "rightRecord",
    "restrictRecords"
})
public class CarParkingSpaceRecord {

    @XmlElement(required = true)
    protected ObjectType object;
    @XmlElement(required = true)
    protected Area params;
    @XmlElement(name = "address_room", required = true)
    protected AddressLocationCarParkingSpace addressRoom;
    @XmlElement(name = "right_record", required = true)
    protected RightRecordIndivid rightRecord;
    @XmlElement(name = "restrict_records")
    protected RestrictRecordsIndividNoPart restrictRecords;

    /**
     * Gets the value of the object property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectType }
     *     
     */
    public ObjectType getObject() {
        return object;
    }

    /**
     * Sets the value of the object property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectType }
     *     
     */
    public void setObject(ObjectType value) {
        this.object = value;
    }

    /**
     * Gets the value of the params property.
     * 
     * @return
     *     possible object is
     *     {@link Area }
     *     
     */
    public Area getParams() {
        return params;
    }

    /**
     * Sets the value of the params property.
     * 
     * @param value
     *     allowed object is
     *     {@link Area }
     *     
     */
    public void setParams(Area value) {
        this.params = value;
    }

    /**
     * Gets the value of the addressRoom property.
     * 
     * @return
     *     possible object is
     *     {@link AddressLocationCarParkingSpace }
     *     
     */
    public AddressLocationCarParkingSpace getAddressRoom() {
        return addressRoom;
    }

    /**
     * Sets the value of the addressRoom property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressLocationCarParkingSpace }
     *     
     */
    public void setAddressRoom(AddressLocationCarParkingSpace value) {
        this.addressRoom = value;
    }

    /**
     * Gets the value of the rightRecord property.
     * 
     * @return
     *     possible object is
     *     {@link RightRecordIndivid }
     *     
     */
    public RightRecordIndivid getRightRecord() {
        return rightRecord;
    }

    /**
     * Sets the value of the rightRecord property.
     * 
     * @param value
     *     allowed object is
     *     {@link RightRecordIndivid }
     *     
     */
    public void setRightRecord(RightRecordIndivid value) {
        this.rightRecord = value;
    }

    /**
     * Gets the value of the restrictRecords property.
     * 
     * @return
     *     possible object is
     *     {@link RestrictRecordsIndividNoPart }
     *     
     */
    public RestrictRecordsIndividNoPart getRestrictRecords() {
        return restrictRecords;
    }

    /**
     * Sets the value of the restrictRecords property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestrictRecordsIndividNoPart }
     *     
     */
    public void setRestrictRecords(RestrictRecordsIndividNoPart value) {
        this.restrictRecords = value;
    }

}
