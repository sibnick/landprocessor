//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.12.21 at 05:58:11 PM GMT+07:00 
//


package extract.kpt;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Точки пересечения проекций
 * 
 * <p>Java class for IntersectionPoints complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IntersectionPoints">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="level_contour" type="{}Dict" minOccurs="0"/>
 *         &lt;element name="intersection_point" type="{}IntersectionPoint" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IntersectionPoints", propOrder = {
    "levelContour",
    "intersectionPoint"
})
public class IntersectionPoints {

    @XmlElement(name = "level_contour")
    protected Dict levelContour;
    @XmlElement(name = "intersection_point", required = true)
    protected List<IntersectionPoint> intersectionPoint;

    /**
     * Gets the value of the levelContour property.
     * 
     * @return
     *     possible object is
     *     {@link Dict }
     *     
     */
    public Dict getLevelContour() {
        return levelContour;
    }

    /**
     * Sets the value of the levelContour property.
     * 
     * @param value
     *     allowed object is
     *     {@link Dict }
     *     
     */
    public void setLevelContour(Dict value) {
        this.levelContour = value;
    }

    /**
     * Gets the value of the intersectionPoint property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the intersectionPoint property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIntersectionPoint().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntersectionPoint }
     * 
     * 
     */
    public List<IntersectionPoint> getIntersectionPoint() {
        if (intersectionPoint == null) {
            intersectionPoint = new ArrayList<IntersectionPoint>();
        }
        return this.intersectionPoint;
    }

}
