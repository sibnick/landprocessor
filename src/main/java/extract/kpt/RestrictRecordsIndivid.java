//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.12.21 at 05:58:11 PM GMT+07:00 
//


package extract.kpt;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Сведения об ограничениях прав и обременениях объекта недвижимости (части (частей) ОН)
 * 
 * <p>Java class for RestrictRecordsIndivid complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RestrictRecordsIndivid">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="restrict_record" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="restrictions_encumbrances_data">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="restriction_encumbrance_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="part_numbers" type="{}PartNumbers" minOccurs="0"/>
 *                             &lt;element name="restriction_encumbrance_type" type="{}Dict"/>
 *                             &lt;element name="restricting_rights" type="{}RestrictingRightsAllNumber" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RestrictRecordsIndivid", propOrder = {
    "restrictRecord"
})
public class RestrictRecordsIndivid {

    @XmlElement(name = "restrict_record", required = true)
    protected List<RestrictRecordsIndivid.RestrictRecord> restrictRecord;

    /**
     * Gets the value of the restrictRecord property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the restrictRecord property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRestrictRecord().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RestrictRecordsIndivid.RestrictRecord }
     * 
     * 
     */
    public List<RestrictRecordsIndivid.RestrictRecord> getRestrictRecord() {
        if (restrictRecord == null) {
            restrictRecord = new ArrayList<RestrictRecordsIndivid.RestrictRecord>();
        }
        return this.restrictRecord;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="restrictions_encumbrances_data">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="restriction_encumbrance_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="part_numbers" type="{}PartNumbers" minOccurs="0"/>
     *                   &lt;element name="restriction_encumbrance_type" type="{}Dict"/>
     *                   &lt;element name="restricting_rights" type="{}RestrictingRightsAllNumber" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "restrictionsEncumbrancesData"
    })
    public static class RestrictRecord {

        @XmlElement(name = "restrictions_encumbrances_data", required = true)
        protected RestrictRecordsIndivid.RestrictRecord.RestrictionsEncumbrancesData restrictionsEncumbrancesData;

        /**
         * Gets the value of the restrictionsEncumbrancesData property.
         * 
         * @return
         *     possible object is
         *     {@link RestrictRecordsIndivid.RestrictRecord.RestrictionsEncumbrancesData }
         *     
         */
        public RestrictRecordsIndivid.RestrictRecord.RestrictionsEncumbrancesData getRestrictionsEncumbrancesData() {
            return restrictionsEncumbrancesData;
        }

        /**
         * Sets the value of the restrictionsEncumbrancesData property.
         * 
         * @param value
         *     allowed object is
         *     {@link RestrictRecordsIndivid.RestrictRecord.RestrictionsEncumbrancesData }
         *     
         */
        public void setRestrictionsEncumbrancesData(RestrictRecordsIndivid.RestrictRecord.RestrictionsEncumbrancesData value) {
            this.restrictionsEncumbrancesData = value;
        }


        /**
         * 
         * 
         *  
         * 
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="restriction_encumbrance_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="part_numbers" type="{}PartNumbers" minOccurs="0"/>
         *         &lt;element name="restriction_encumbrance_type" type="{}Dict"/>
         *         &lt;element name="restricting_rights" type="{}RestrictingRightsAllNumber" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "restrictionEncumbranceNumber",
            "partNumbers",
            "restrictionEncumbranceType",
            "restrictingRights"
        })
        public static class RestrictionsEncumbrancesData {

            @XmlElement(name = "restriction_encumbrance_number", required = true)
            protected String restrictionEncumbranceNumber;
            @XmlElement(name = "part_numbers")
            protected PartNumbers partNumbers;
            @XmlElement(name = "restriction_encumbrance_type", required = true)
            protected Dict restrictionEncumbranceType;
            @XmlElement(name = "restricting_rights")
            protected RestrictingRightsAllNumber restrictingRights;

            /**
             * Gets the value of the restrictionEncumbranceNumber property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRestrictionEncumbranceNumber() {
                return restrictionEncumbranceNumber;
            }

            /**
             * Sets the value of the restrictionEncumbranceNumber property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRestrictionEncumbranceNumber(String value) {
                this.restrictionEncumbranceNumber = value;
            }

            /**
             * Gets the value of the partNumbers property.
             * 
             * @return
             *     possible object is
             *     {@link PartNumbers }
             *     
             */
            public PartNumbers getPartNumbers() {
                return partNumbers;
            }

            /**
             * Sets the value of the partNumbers property.
             * 
             * @param value
             *     allowed object is
             *     {@link PartNumbers }
             *     
             */
            public void setPartNumbers(PartNumbers value) {
                this.partNumbers = value;
            }

            /**
             * Gets the value of the restrictionEncumbranceType property.
             * 
             * @return
             *     possible object is
             *     {@link Dict }
             *     
             */
            public Dict getRestrictionEncumbranceType() {
                return restrictionEncumbranceType;
            }

            /**
             * Sets the value of the restrictionEncumbranceType property.
             * 
             * @param value
             *     allowed object is
             *     {@link Dict }
             *     
             */
            public void setRestrictionEncumbranceType(Dict value) {
                this.restrictionEncumbranceType = value;
            }

            /**
             * Gets the value of the restrictingRights property.
             * 
             * @return
             *     possible object is
             *     {@link RestrictingRightsAllNumber }
             *     
             */
            public RestrictingRightsAllNumber getRestrictingRights() {
                return restrictingRights;
            }

            /**
             * Sets the value of the restrictingRights property.
             * 
             * @param value
             *     allowed object is
             *     {@link RestrictingRightsAllNumber }
             *     
             */
            public void setRestrictingRights(RestrictingRightsAllNumber value) {
                this.restrictingRights = value;
            }

        }

    }

}
