//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.12.21 at 05:58:11 PM GMT+07:00 
//


package extract.kpt;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Сведения о характеристиках земельного участка (и сведения о создании (эксплуатации) на земельном участке наемного дома)
 * 
 * <p>Java class for LandRecordApartmentBuilding complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LandRecordApartmentBuilding">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="object" type="{}ObjectAndWithdrawalState"/>
 *         &lt;element name="apartment_building" type="{}ApartmentBuilding" minOccurs="0"/>
 *         &lt;element name="params" type="{}ParamsLandCategoryUses"/>
 *         &lt;element name="address_location" type="{}AddressLocationLand"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LandRecordApartmentBuilding", propOrder = {
    "object",
    "apartmentBuilding",
    "params",
    "addressLocation"
})
public class LandRecordApartmentBuilding {

    @XmlElement(required = true)
    protected ObjectAndWithdrawalState object;
    @XmlElement(name = "apartment_building")
    protected ApartmentBuilding apartmentBuilding;
    @XmlElement(required = true)
    protected ParamsLandCategoryUses params;
    @XmlElement(name = "address_location", required = true)
    protected AddressLocationLand addressLocation;

    /**
     * Gets the value of the object property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectAndWithdrawalState }
     *     
     */
    public ObjectAndWithdrawalState getObject() {
        return object;
    }

    /**
     * Sets the value of the object property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectAndWithdrawalState }
     *     
     */
    public void setObject(ObjectAndWithdrawalState value) {
        this.object = value;
    }

    /**
     * Gets the value of the apartmentBuilding property.
     * 
     * @return
     *     possible object is
     *     {@link ApartmentBuilding }
     *     
     */
    public ApartmentBuilding getApartmentBuilding() {
        return apartmentBuilding;
    }

    /**
     * Sets the value of the apartmentBuilding property.
     * 
     * @param value
     *     allowed object is
     *     {@link ApartmentBuilding }
     *     
     */
    public void setApartmentBuilding(ApartmentBuilding value) {
        this.apartmentBuilding = value;
    }

    /**
     * Gets the value of the params property.
     * 
     * @return
     *     possible object is
     *     {@link ParamsLandCategoryUses }
     *     
     */
    public ParamsLandCategoryUses getParams() {
        return params;
    }

    /**
     * Sets the value of the params property.
     * 
     * @param value
     *     allowed object is
     *     {@link ParamsLandCategoryUses }
     *     
     */
    public void setParams(ParamsLandCategoryUses value) {
        this.params = value;
    }

    /**
     * Gets the value of the addressLocation property.
     * 
     * @return
     *     possible object is
     *     {@link AddressLocationLand }
     *     
     */
    public AddressLocationLand getAddressLocation() {
        return addressLocation;
    }

    /**
     * Sets the value of the addressLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressLocationLand }
     *     
     */
    public void setAddressLocation(AddressLocationLand value) {
        this.addressLocation = value;
    }

}
