//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.12.21 at 05:58:11 PM GMT+07:00 
//


package extract.kpt;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Иное лицо, в пользу которого установлены ограничения права и обременения объекта недвижимости
 * 
 * <p>Java class for AnotherRestricted complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AnotherRestricted">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="another_type" type="{}AnotherTypeRestricted"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AnotherRestricted", propOrder = {
    "anotherType"
})
public class AnotherRestricted {

    @XmlElement(name = "another_type", required = true)
    protected AnotherTypeRestricted anotherType;

    /**
     * Gets the value of the anotherType property.
     * 
     * @return
     *     possible object is
     *     {@link AnotherTypeRestricted }
     *     
     */
    public AnotherTypeRestricted getAnotherType() {
        return anotherType;
    }

    /**
     * Sets the value of the anotherType property.
     * 
     * @param value
     *     allowed object is
     *     {@link AnotherTypeRestricted }
     *     
     */
    public void setAnotherType(AnotherTypeRestricted value) {
        this.anotherType = value;
    }

}
