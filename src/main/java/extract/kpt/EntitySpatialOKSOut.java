//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.12.21 at 05:58:11 PM GMT+07:00 
//


package extract.kpt;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Описание элементов котнура
 * 
 * <p>Java class for EntitySpatialOKSOut complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EntitySpatialOKSOut">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sk_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="spatials_elements" type="{}SpatialsElementsOKSOut"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EntitySpatialOKSOut", propOrder = {
    "skId",
    "spatialsElements"
})
public class EntitySpatialOKSOut {

    @XmlElement(name = "sk_id")
    protected String skId;
    @XmlElement(name = "spatials_elements", required = true)
    protected SpatialsElementsOKSOut spatialsElements;

    /**
     * Gets the value of the skId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSkId() {
        return skId;
    }

    /**
     * Sets the value of the skId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSkId(String value) {
        this.skId = value;
    }

    /**
     * Gets the value of the spatialsElements property.
     * 
     * @return
     *     possible object is
     *     {@link SpatialsElementsOKSOut }
     *     
     */
    public SpatialsElementsOKSOut getSpatialsElements() {
        return spatialsElements;
    }

    /**
     * Sets the value of the spatialsElements property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpatialsElementsOKSOut }
     *     
     */
    public void setSpatialsElements(SpatialsElementsOKSOut value) {
        this.spatialsElements = value;
    }

}
