//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.12.21 at 05:58:11 PM GMT+07:00 
//


package extract.kpt;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Сведения о праве
 * 
 * <p>Java class for RightRecordIndivid complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RightRecordIndivid">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="record_info" type="{}RecordInfo"/>
 *         &lt;element name="right_data" type="{}RightDataAndCancel"/>
 *         &lt;element name="underlying_documents" type="{}UnderlyingDocumentsOut" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RightRecordIndivid", propOrder = {
    "recordInfo",
    "rightData",
    "underlyingDocuments"
})
public class RightRecordIndivid {

    @XmlElement(name = "record_info", required = true)
    protected RecordInfo recordInfo;
    @XmlElement(name = "right_data", required = true)
    protected RightDataAndCancel rightData;
    @XmlElement(name = "underlying_documents")
    protected UnderlyingDocumentsOut underlyingDocuments;

    /**
     * Gets the value of the recordInfo property.
     * 
     * @return
     *     possible object is
     *     {@link RecordInfo }
     *     
     */
    public RecordInfo getRecordInfo() {
        return recordInfo;
    }

    /**
     * Sets the value of the recordInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link RecordInfo }
     *     
     */
    public void setRecordInfo(RecordInfo value) {
        this.recordInfo = value;
    }

    /**
     * Gets the value of the rightData property.
     * 
     * @return
     *     possible object is
     *     {@link RightDataAndCancel }
     *     
     */
    public RightDataAndCancel getRightData() {
        return rightData;
    }

    /**
     * Sets the value of the rightData property.
     * 
     * @param value
     *     allowed object is
     *     {@link RightDataAndCancel }
     *     
     */
    public void setRightData(RightDataAndCancel value) {
        this.rightData = value;
    }

    /**
     * Gets the value of the underlyingDocuments property.
     * 
     * @return
     *     possible object is
     *     {@link UnderlyingDocumentsOut }
     *     
     */
    public UnderlyingDocumentsOut getUnderlyingDocuments() {
        return underlyingDocuments;
    }

    /**
     * Sets the value of the underlyingDocuments property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnderlyingDocumentsOut }
     *     
     */
    public void setUnderlyingDocuments(UnderlyingDocumentsOut value) {
        this.underlyingDocuments = value;
    }

}
