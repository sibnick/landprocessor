//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.12.21 at 05:58:11 PM GMT+07:00 
//


package extract.kpt;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Сведения об объекте недвижимости - сооружении, объекте незавершенного строительства, едином недвижимом комплексе (вид, кадастровый номер, адрес полный)
 * 
 * <p>Java class for DataObjectConstruction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DataObjectConstruction">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="object" type="{}ObjectType"/>
 *         &lt;element name="address_location" type="{}AddressLocationConstruction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataObjectConstruction", propOrder = {
    "object",
    "addressLocation"
})
public class DataObjectConstruction {

    @XmlElement(required = true)
    protected ObjectType object;
    @XmlElement(name = "address_location")
    protected AddressLocationConstruction addressLocation;

    /**
     * Gets the value of the object property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectType }
     *     
     */
    public ObjectType getObject() {
        return object;
    }

    /**
     * Sets the value of the object property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectType }
     *     
     */
    public void setObject(ObjectType value) {
        this.object = value;
    }

    /**
     * Gets the value of the addressLocation property.
     * 
     * @return
     *     possible object is
     *     {@link AddressLocationConstruction }
     *     
     */
    public AddressLocationConstruction getAddressLocation() {
        return addressLocation;
    }

    /**
     * Sets the value of the addressLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressLocationConstruction }
     *     
     */
    public void setAddressLocation(AddressLocationConstruction value) {
        this.addressLocation = value;
    }

}
