//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.12.21 at 05:58:11 PM GMT+07:00 
//


package extract.kpt;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Описание местоположения машино-места
 * 
 * <p>Java class for SpecificLocation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SpecificLocation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="specific_mark_distances" type="{}SpecificMarkDistances"/>
 *         &lt;element name="character_point_distances" type="{}CharacterPointDistances"/>
 *         &lt;element name="specific_mark_location" type="{}SpecificMarkLocation" minOccurs="0"/>
 *         &lt;element name="room_location" type="{}RoomLocation" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpecificLocation", propOrder = {
    "specificMarkDistances",
    "characterPointDistances",
    "specificMarkLocation",
    "roomLocation"
})
public class SpecificLocation {

    @XmlElement(name = "specific_mark_distances", required = true)
    protected SpecificMarkDistances specificMarkDistances;
    @XmlElement(name = "character_point_distances", required = true)
    protected CharacterPointDistances characterPointDistances;
    @XmlElement(name = "specific_mark_location")
    protected SpecificMarkLocation specificMarkLocation;
    @XmlElement(name = "room_location")
    protected RoomLocation roomLocation;

    /**
     * Gets the value of the specificMarkDistances property.
     * 
     * @return
     *     possible object is
     *     {@link SpecificMarkDistances }
     *     
     */
    public SpecificMarkDistances getSpecificMarkDistances() {
        return specificMarkDistances;
    }

    /**
     * Sets the value of the specificMarkDistances property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpecificMarkDistances }
     *     
     */
    public void setSpecificMarkDistances(SpecificMarkDistances value) {
        this.specificMarkDistances = value;
    }

    /**
     * Gets the value of the characterPointDistances property.
     * 
     * @return
     *     possible object is
     *     {@link CharacterPointDistances }
     *     
     */
    public CharacterPointDistances getCharacterPointDistances() {
        return characterPointDistances;
    }

    /**
     * Sets the value of the characterPointDistances property.
     * 
     * @param value
     *     allowed object is
     *     {@link CharacterPointDistances }
     *     
     */
    public void setCharacterPointDistances(CharacterPointDistances value) {
        this.characterPointDistances = value;
    }

    /**
     * Gets the value of the specificMarkLocation property.
     * 
     * @return
     *     possible object is
     *     {@link SpecificMarkLocation }
     *     
     */
    public SpecificMarkLocation getSpecificMarkLocation() {
        return specificMarkLocation;
    }

    /**
     * Sets the value of the specificMarkLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpecificMarkLocation }
     *     
     */
    public void setSpecificMarkLocation(SpecificMarkLocation value) {
        this.specificMarkLocation = value;
    }

    /**
     * Gets the value of the roomLocation property.
     * 
     * @return
     *     possible object is
     *     {@link RoomLocation }
     *     
     */
    public RoomLocation getRoomLocation() {
        return roomLocation;
    }

    /**
     * Sets the value of the roomLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link RoomLocation }
     *     
     */
    public void setRoomLocation(RoomLocation value) {
        this.roomLocation = value;
    }

}
